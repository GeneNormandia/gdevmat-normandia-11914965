class Walker
{
  public PVector position;
  public PVector direction;
  public float Red;
  public float Green;
  public float Blue;
  public float ts = random(50);
  float scale = 50; 
    
  //Randomize the colors
  void colors ()
  {
    Red = random(255);
    Green = random(255);
    Blue = random(255);
    fill (Red, Green, Blue);
  }

  //Spawn a black hole
  void renderBlackHole()
  {
    fill(255,255,255);
    circle(blackHolePosition().x, blackHolePosition().y, scale);
    position = blackHolePosition();
  }
  
  //Spawn circles
  void renderCircles()
  {
    perlinScale();
    colors();
    circle(circlesPosition().x, circlesPosition().y, scale);
    position = circlesPosition();
  }
  
  //Randomized position of black hole using vector
  PVector blackHolePosition()
  {
    float x = random(Window.left, Window.right);
    float y = random(Window.bottom, Window.top);
    return new PVector(x,y);
  }
  
  //Randomized position of circles using vector
  PVector circlesPosition()
  {
    float gaussian = randomGaussian();
    float standardDeviation = 200; //spread
    float mean = 0; //position of circles
    float x = standardDeviation * gaussian + mean;
    float y = random(Window.bottom, Window.top);
    noStroke();
    return new PVector(x,y);
  }
  
  //Randomize the scale of the circles using perlin
  void perlinScale()
  {  
    scale = map(noise(ts), 0, 1, 5, 50);
  }
}
