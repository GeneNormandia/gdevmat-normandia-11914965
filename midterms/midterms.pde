Walker[] circles = new Walker[100];
Walker walker = new Walker();

PVector mousePosition()
{
  float x = mouseX - Window.windowWidth / 2;
  float y = -(mouseY - Window.windowHeight / 2);
  return new PVector (x,y);
}

void setup()
{
   size(1280, 720, P3D);
   camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
   background(0);
   noStroke();  
   
   walker.renderBlackHole();
  
   for (int i = 0; i < circles.length; i++)
   {
     circles[i] = new Walker();
     circles[i].renderCircles();
   }
}

public float counter = 0;

void draw ()
{
  //Run for 150 frames
  if(counter < 150)
  {
    background(0);
    suck();
    followMouse();
    fill(255);
    circle(walker.position.x, walker.position.y, walker.scale);
    counter += 1;
    println(counter);
  }
   //Reset when frame counter reach 150
   else if (counter >= 150) 
  {
    walker.renderBlackHole();
      for (int i = 0; i < circles.length; i++)
      {
         circles[i] = new Walker();
         circles[i].renderCircles();
      }
    background(0);
    counter = 0;
  }
}

//Circles movement
void suck()
{
  for (int i = 0; i < circles.length; i++)
  {  
     float distance = circles[i].position.dist(walker.position); //Distance checker
     PVector currentDirection = PVector.sub(walker.position, circles[i].position);
     circles[i].direction = currentDirection;
     circles[i].direction.normalize().mult(8);
     circles[i].position.add(circles[i].direction);
     fill(circles[i].Red, circles[i].Green, circles[i].Blue);
     circle(circles[i].position.x, circles[i].position.y, circles[i].scale);
     
     //Change the scale of the circles for it to disappear once it gets close to the black hole
     if (distance < 8)
     {
       circles[i].scale = 0;
       circle(circles[i].position.x, circles[i].position.y, circles[i].scale);
     }
  }
}

//Make the black hole follow the mouse
void followMouse()
{
  PVector mouse = mousePosition(); 
  PVector currentDirection = PVector.sub(mouse, walker.position);
  
  //Ease the speed of the black hole when it comes near the circle
  float easing = 0.05;
  float targetX = mouse.x;
  float dx = targetX - walker.position.x;
  float targetY = mouse.y;
  float dy = targetY - walker.position.y;
  
  walker.position.x += dx * easing;
  walker.position.y += dy * easing;
  walker.direction = currentDirection;
  walker.direction.normalize();
  walker.position.add(walker.direction);
}
