float x;
int counter = 0;
void setup()
{
  size(1280, 720, P3D);
  camera(0, 0, -(height/2.0) / tan(PI*30.0 / 180.0), 0, 0, 0, 0, -1, 0);
  x = -300;
  background(0);
}

void draw()
{
  if(counter <= 120)
  {
  drawSineWave();
  drawCartesianPlane();
  drawLinearFunction();
  drawQuadraticFunction();
  }
  else if(counter >= 120)
  {
  drawCartesianPlane();
  drawLinearFunction();
  drawQuadraticFunction();
  drawSineWave();
  background(0);
  counter = 0;
  x = -300;
  }
  counter += 1;
  println(counter);
}

void drawSineWave()
{
  stroke(255, 100, 0);
  strokeWeight(5);
  point(x,0+30*sin(x/30));
  x=x+5;
}

void drawCartesianPlane()
{
  strokeWeight(1);
  color white = color(255, 255, 255);
  fill(white);
  stroke(white);
  line(300, 0, -300, 0);
  line(0, 300, 0, -300);
  
  for(int i = -300; i <= 300; i+=10)
  {
    line(i, -2, i, 2);
    line(-2, i, 2, i);
  }
}

void drawLinearFunction()
{
  color purple = color(200, 0, 200);
  fill(purple);
  noStroke();
  
  for(int x = -200; x <= 200; x++)
  {
    circle(x, (-5) * (x) + 30, 5);
  }
}

void drawQuadraticFunction()
{
  color yellow = color(200, 200, 0);
  fill(yellow);
  stroke(yellow);
  
  for(float x = -300; x <= 300; x+=0.1f)
  {
    circle(x, ((float)Math.pow(x,2) - (x * 15) - 3), 5);
  }
}
