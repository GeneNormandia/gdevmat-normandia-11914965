class Walker
{
  float x;
  float y;
  
  void render()
  {
     circle(x, y, 30);
  }
  
  void randomWalk()
  {
    int rng = int(random(8));
    
    if (rng == 0) //move up
    {
      fill(150,0,0,random(50)+50); //red 
      stroke(0, 0, 0, 0);
      y+=10;
    }
    
    else if (rng == 1) // move down
    {
      fill(0,150,0, random(50)+50 );//green
      stroke(0, 0, 0, 0);
      y-=10;
    }
    else if (rng == 2) //move right
    {
      fill(0,0,150,random(50)+50 );//blue
      stroke(0, 0, 0, 0);
      x+=10;
    }
    else if (rng == 3) //move left
    {
      fill(150,150,0,random(50)+50 );//yellow
      stroke(0, 0, 0, 0);
      x-=10;
    }
    
     else if (rng == 4) //move top left
    {
      fill(150,0,150,random(50)+50 );
      stroke(0, 0, 0, 0);
      y+=10;
      x-=10;
    }
    
      else if (rng == 5) //move top right
    {
      fill(0,150,150,random(50)+50 );
      stroke(0, 0, 0, 0);
      y+=10;
      x+=10;
    }
      else if (rng == 6) //move bottom left
    {
      fill(75,75,0,random(50)+50 );
      stroke(0, 0, 0, 0);
      y-=10;
      x-=10;
    }
      else if (rng == 7) //move bottom right
    {
      fill(75,0,75,random(50)+50 );
      stroke(0, 0, 0, 0);
      y-=10;
      x+=10;
    }
    
  }
  
}
