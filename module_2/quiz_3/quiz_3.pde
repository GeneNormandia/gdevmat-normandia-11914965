void setup()
{
  size(1280, 720, P3D);
  camera(0, 0, -(height/2.0) / tan(PI*30.0 / 180.0), 0, 0, 0, 0, -1, 0);
  background (255);

}
public float counter = 0;
void draw ()
{
  if(counter < 300)
  {
  float gaussian = randomGaussian();
  println(gaussian);
  float standardDeviation = 200; //spread
  float mean = 0; //position of circles
  
  float x = standardDeviation * gaussian + mean;
  float y = random(-360, 360);
  noStroke();
  
  fill(random(255),random(255),random(255), 100);
  circle(x, y, random(30));
  counter += 1;
  }
  else if (counter >= 300) //when frame counter reach 300, flush the screen
  {
    background(255);
    counter = 0;
  }
}
