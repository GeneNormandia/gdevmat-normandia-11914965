public class Walker
{
  public float x;
  public float y;
  public float Red;
  public float Green;
  public float Blue;
  public float tx = 0, ty = 10000;
  public float  tr = 50;
  public float  tg = 100;
  public float  tb = 150;
  public float  t = 0;
  public int red, green, blue;
  public int rand = 255;
  
  void render()
  {
     float s = noise(t);

     float scale = map(s, 0, 1, 5, 150);
     
     circle(x, y, scale);
     
     println("Red" + Red + "Green" + Green + "Blue" + Blue);
  }
  
  void perlinWalk()
  {  
    fill(Red,Green,Blue,255);
    stroke(1,10,1,1);
    Red = map(noise(tr), 0, 1, 0, 255);
    Green = map(noise(tg), 0, 1, 0, 255);
    Blue = map(noise(tb), 0, 1, 0, 255);
    x = map(noise(tx), 0, 1, -640, 640);
    y = map(noise(ty), 0, 1, -360, 360);

 
    tr += 0.01f; 
    tg += 0.01f; 
    tb += 0.01f;
    t += 0.01f;
    tx += 0.01f;
    ty += 0.01f;
  }



}
