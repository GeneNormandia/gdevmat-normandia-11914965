void setup()
{
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
   size(1280, 720, P3D);

}

PVector mousePos()
{
  float x = mouseX - Window.windowWidth / 2;
  float y = -(mouseY - Window.windowHeight / 2);
  return new PVector(x, y);
}


void draw()
{
   PVector mouse = mousePos();
  background(130);
  
  //Outer glow
  strokeWeight(12);
  stroke(255, 0, 0);
  mouse.normalize().mult(300); 
  line(0, 0, mouse.x, mouse.y);
  
  
  //Innner glow
  strokeWeight(3);
  stroke(255, 255, 255);
  mouse.normalize().mult(300);
  line(0, 0, mouse.x, mouse.y);
  
 //handle
  strokeWeight(12);
  stroke(0, 0, 0);
  mouse.normalize().mult(100);
  line(-mouse.x, -mouse.y, 0, 0);
  
 //sides
 rotate(7.85);
 strokeWeight(12);
 stroke(255, 0, 0);
 mouse.normalize().mult(50); 
 line(-mouse.x, -mouse.y, mouse.x, mouse.y);


 //sides inner glow
 strokeWeight(3);
 stroke(255, 255, 255);
 mouse.normalize().mult(50); 
 line(-mouse.x, -mouse.y, mouse.x, mouse.y);

 //side handle
 strokeWeight(12);
 stroke(0, 0, 0);
 mouse.normalize().mult(25);
 line(-mouse.x, -mouse.y, mouse.x, mouse.y);
  
}
