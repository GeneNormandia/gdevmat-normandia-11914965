public class Walker
{
  PVector position = new PVector();
 
  void render()
  {   
     circle(position.x, position.y, 50);
  }
  
  void perlinWalk()
  {  
    fill(182,52,100);
  }

  void moveAndBounce()
  {
      position.add(speed);
  
    if ((position.x > Window.right) || (position.x < Window.left))
    {
      speed.x *= -1;
    }
  
    if ((position.y > Window.top) || (position.y < Window.bottom))
    {
      speed.y *= -1;
    }
  }

}
