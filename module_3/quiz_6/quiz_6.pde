void setup()
{
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
    size(1280, 720, P3D);

}

PVector mousePos()
{
  float x = mouseX - Window.windowWidth / 2;
  float y = -(mouseY - Window.windowHeight / 2);
  return new PVector(x, y);
}


void draw()
{
  PVector mouse = mousePos();
  float x = mouse.x;
  float y = mouse.y;
  
  background(130);
  println(mag(x,y));
  
  //Outer glow
  strokeWeight(12);
  stroke(255, 0, 0);
  mouse.mult(1);
  //mouse.normalize().mult(300); 
  line(-mouse.x, -mouse.y, mouse.x, mouse.y);
  
  //Innner glow
  strokeWeight(3);
  stroke(255, 255, 255);
  mouse.mult(1);
 //mouse.normalize().mult(300);
  line(-mouse.x, -mouse.y, mouse.x, mouse.y);
  
  //Handle
  strokeWeight(10);
  stroke(0, 0, 0);
  mouse.normalize().mult(50);
  line(-mouse.x, -mouse.y, mouse.x, mouse.y);
}
