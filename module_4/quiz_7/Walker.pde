public class Walker 
{
  public PVector position = new PVector();
  public PVector velocity= new PVector();
  public PVector acceleration = new PVector();
  public PVector direction = new PVector();
  
  public float velocityLimit = 10;
  public float scale = random(5, 10);
  
  PVector randomPosition = new PVector(random(Window.left, Window.right + 1), random(Window.bottom, Window.top + 1));
  
  public Walker()
  {
  }
  //Update the position of the circles
  public void update()
  {
    PVector mouse = new PVector( mouseX - Window.windowWidth / 2, -(mouseY - Window.windowHeight / 2));
    PVector currentDirection = PVector.sub(mouse, this.position);
    this.direction = currentDirection;
    direction.normalize();
    acceleration = direction;
    acceleration.mult(0.2);
    velocity.add(acceleration);
    velocity.limit(velocityLimit);
    position.add(velocity);
  }
  //Render the circles
  public void render()
  {
   circle(randomPosition.x, randomPosition.y, scale);
   position = randomPosition;
  }
}
  
