Walker[] w = new Walker[100];
Walker myWalker = new Walker();

void setup()
{
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  size(1280, 720, P3D);
  background(80);
   for (int i = 0; i < w.length; i++)
 {
   w[i] = new Walker();
   w[i].render();
 }
}

void draw()
{
  background(80);
     for (int i = 0; i < w.length; i++)
 {
   w[i].update();
   w[i].render();
 }
}
