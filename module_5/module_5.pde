Walker[] circles = new Walker [10];
Walker walker = new Walker();

PVector wind = new PVector(0.15, 0);
PVector gravity = new PVector(0, -0.4);

void setup()
{
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  size(1280, 720, P3D);
  noStroke();  
 //Render 10 circles with different mass and scales
 for (int i = 0; i < circles.length; i++)
 {
   circles[i] = new Walker();
   circles[i].red = random(255);
   circles[i].green = random(255);
   circles[i].blue = random(255);
   circles[i].render();
   circles[i].mass = 10 - i;
   circles[i].scale = circles[i].mass * 15;
 }
}

void draw()
{
  background(80);
  //Update the location of the circles every frame
   for (int i = 0; i < circles.length; i++)
 {
   circles[i].render();
   circles[i].update();
   circles[i].applyForce(wind);
   circles[i].applyForce(gravity);
   circles[i].checkEdges();
 }
}
