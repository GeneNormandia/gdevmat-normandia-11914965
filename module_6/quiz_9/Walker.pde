public class Walker 
{
  public PVector position = new PVector(-500, 200);
  public PVector velocity= new PVector();
  public PVector acceleration = new PVector();
  public float red, green, blue;
  public float velocityLimit = 10;
  public float scale= 1;
  public float mass = 1;
  
  public Walker()
  {
    
  }
  
  //Apply force to the circle
  public void applyForce (PVector force)
  {
     PVector f = PVector.div(force, this.mass);
     this.acceleration.add(f); 
  }
  
  //Modify the velocity and acceleration of the circle
  public void update()
  {
    this.velocity.add(this.acceleration);
    this.velocity.limit(velocityLimit);
    this.position.add(this.velocity);
    this.acceleration.mult(0);
  }
  
  public void render()
  {
    circle(position.x, position.y, scale);
    fill(red, green, blue);
    //colors();
  }
}
  
