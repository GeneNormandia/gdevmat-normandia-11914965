Walker[] circles = new Walker [8];
Walker walker = new Walker();

void setup()
{
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  size(1280, 720, P3D);
  noStroke();  
  int posY = 0;
 //Render 8 circles with different mass and scales
 for (int i = 0; i < circles.length; i++)
 {
   posY = 2 * (Window.windowHeight / 8) * (i - 4);
   circles[i] = new Walker();
   circles[i].position = new PVector(-500, posY);
   circles[i].red = random(255);
   circles[i].green = random(255);
   circles[i].blue = random(255);
   circles[i].render();
   circles[i].mass = 9 - i;
   circles[i].scale = circles[i].mass * 10;
 }
}

void draw()
{
  noStroke();  
  background(80);
  //Update the location of the circles every frame
   for (Walker w : circles)
 {
   //Friction = -1 * mew * N * v
   float mew = 0.01f; //coefficient of friction 
       if (w.position.x > 0)
    {
      mew = 0.4f;
    }
   float normal = 1;
   float frictionMagnitude = mew  * normal;
   PVector friction = w.velocity.copy();
   friction. mult(-1);
   friction.normalize();
   friction.mult(frictionMagnitude);
   PVector acceleration = new PVector(0.2, 0);
   w.render();
   w.update();
   w.applyForce(acceleration);
   w.applyForce(friction);
    println(mew);
 }
 strokeWeight(4);
 stroke(0);
 line(0, Window.top, 0, Window.bottom);
}
//Reset enerything when the mouse is pressed
void mousePressed()
{
   setup();
}
