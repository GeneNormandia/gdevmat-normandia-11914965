public class Walker 
{
  public PVector position = new PVector();
  public PVector velocity= new PVector();
  public PVector acceleration = new PVector();
  

  public float velocityLimit = 10;
  public float scale= 15;
  public float mass = 1;
  
  //public float red = 255, green = 255, blue = 255, a = 255;
  public float red, green , blue;
  
  public Walker()
  {
    
  }
  //Apply force to the circle
  public void applyForce (PVector force)
  {
     PVector f = PVector.div(force, this.mass);
     this.acceleration.add(f); 
  }
  //Modify the velocity and acceleration of the circle
  public void update()
  {
    this.velocity.add(this.acceleration);
    this.velocity.limit(velocityLimit);
    this.position.add(this.velocity);
    this.acceleration.mult(0);
  }
  
  public void render()
  {
    fill(red, green, blue);
    circle(position.x, position.y, scale);
    
  }

  //Make the circle bounce off the edges of the screen
  public void checkEdges()
  {
    if (this.position.x > Window.right)
    {
      this.velocity.x *= -1;
    }
    else if (this.position.x < Window.left)
    {
      this.velocity.x *= -1;
    }
    
    if (this.position.y > Window.top)
    {
      this.velocity.y *= -1;
    }
    if (this.position.y < Window.bottom)
    {
       this.velocity.y *= -1;
    }
  }
}
  
