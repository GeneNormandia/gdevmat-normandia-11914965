Liquid ocean = new Liquid (0, -100,Window.right,Window.bottom, 0.1f);
Walker[] w = new Walker[10];
Walker myWalker = new Walker();

void setup()
{
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  size(1280, 720, P3D);
  noStroke();  
 int posX = 0;
 //Render 8 circles with different mass and scales
 for (int i = 0; i < w.length; i++)
 {
   posX = 2 * (Window.windowHeight / 10) * (i - 5);
   w[i] = new Walker();
   w[i].position = new PVector(posX, 300);
   w[i].red = random(255);
   w[i].green = random(255);
   w[i].blue = random(255);
   w[i].render();
   w[i].mass = random(1, 10); //Randomized mass
   w[i].scale = w[i].mass * 10; //Scale according to size
 }
 }

void draw()
{
  background(255);
  //Render the ocean
  ocean.render();
  //Render the circles
   for (int i = 0; i < w.length; i++)
 {
   float c = 0.1f;
   float normal = 1;
   float frictionMagnitude = c * normal;
   PVector friction = w[i].velocity.copy();  
   PVector wind = new PVector(0.1, 0);
   PVector gravity = new PVector(0, -0.15 * w[i].mass);
    if(ocean.isCollidingWith(w[i]))
  {
    PVector dragForce = ocean.calculateDragForce(w[i]);
    w[i].applyForce(dragForce);
    wind.x = 0;
    wind.y = 0;
  }
   w[i].render();
   w[i].update();
   w[i].applyForce(gravity);
   w[i].applyForce(wind);
   w[i].applyForce(friction.mult(-1).normalize().mult(frictionMagnitude));
 
  if(w[i].position.y <= Window.bottom)
  {
    w[i].position.y = Window.bottom;
    w[i].velocity.y *= -1;
  }

 }
}
void mousePressed()
{
   setup();
}
