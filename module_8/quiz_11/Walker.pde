public class Walker 
{
  public PVector position = new PVector();
  public PVector velocity= new PVector();
  public PVector acceleration = new PVector();

  public float velocityLimit = 10;
  public float scale= 15;
  public float mass = 1;
  
  PVector randomPosition = new PVector(random(Window.left, Window.right + 1), random(Window.bottom, Window.top + 1));
  public float red, green , blue;
  public float gravitationalConstant = 1;
  
  public Walker()
  {
    
  }
  //Apply force to the circle
  public void applyForce (PVector force)
  {
     PVector f = PVector.div(force, this.mass);
     this.acceleration.add(f); 
  }
  //Modify the velocity and acceleration of the circle
  public void update()
  {
    this.velocity.add(this.acceleration);
    this.velocity.limit(velocityLimit);
    this.position.add(this.velocity);
    this.acceleration.mult(0);
  }
  
  public void render()
  {
    fill(red, green, blue, 90);
    circle(randomPosition.x, randomPosition.y, scale);
    position = randomPosition;
  }
  
  public PVector calculateAttraction(Walker walker)
  {
    PVector force = PVector.sub(this.position, walker.position);
    float distance = force.mag();
    force.normalize();
    
    distance = constrain(distance, 5, 25);
    
    float strength = (this.gravitationalConstant * this.mass * walker.mass) / (distance * distance);
    force.mult(strength);
    return force;
  }
  
}
  
