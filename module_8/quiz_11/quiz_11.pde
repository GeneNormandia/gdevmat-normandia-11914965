Walker[] w = new Walker[10];
Walker walker = new Walker();
Walker bigMatter = new Walker();
Walker smallMatter = new Walker(); 
void setup()
{
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  size(1280, 720, P3D);
  noStroke();  
  
   for (int i = 0; i < w.length; i++)
 {
   w[i] = new Walker();
   w[i].red = random(255);
   w[i].green = random(255);
   w[i].blue = random(255);
   w[i].mass = random(5, 20); //Randomized mass
   w[i].scale = w[i].mass * 10; //Scale according to size
   w[i].render();
 }
}    

void draw()
{
   background(255);
   
   for (Walker walker : w)
   {
     walker.update();
     walker.render();
     for (Walker walker1 : w)
     {
       if (walker != walker1)
       {
         walker.applyForce(walker1.calculateAttraction(walker));
       }
     }
   }
}

void mousePressed()
{
   setup();
}
